<?php

/*
 * This file is part of sineos-filemanager-bundle.
 *
 * (c) Stephan Gehle
 *
 * @license LGPL-3.0-or-later
 */

namespace Sineos\FileManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FileManagerBundle extends Bundle
{
}
