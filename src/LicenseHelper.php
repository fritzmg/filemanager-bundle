<?php

/*
 * This file is part of sineos-filemanager-bundle.
 *
 * (c) Stephan Gehle
 *
 * @license LGPL-3.0-or-later
 */

namespace Sineos\FileManagerBundle;

use Contao\Config;

class LicenseHelper {
	
	static private $validLicenseChecksums = array(
		'd0fc6ed8a4d11e8728238ac802a7ee5d', '2e01cbb2d857700957474bbb64732010','be4f0fb0bde8ecee41cbf590e050bbb3','77273d47056cf4025877eb8a1817bab8','ec4c61a69daaac215144d92a5d5ac90b','53821aa2b2892d2197b92e5fe3486304','ce49827a90adb432cb8eb0ab654d7bd4','f06342a0cb58cff8c5c0bbebdd14b5fa','3526b88c3b5d7f7d008eeb2c5fcc996d','35d2459008d9b0ad1e6fa5bda8ed2129','3ef4cdf647103ea35ff59a99940cf060','6653cfe35a2a13968817b84844fcb236','9346c5d7c28fbb2127919876ed8f2e56','3173975cd857e5ac60afcd8d3bf96a6b','dcffd56148bebfc97e19b21149564a2e','583684075c925f7f650fc65ad9e8e69e','7d280625223df75f87a95c767ebc1074','69bcf4aa9686e4891de312a55df7bbe4','8c2f73fc11fd88647ee8202c8f0f544f','b3abf4de4f95b00a43f4eec7053a4a01');
	
	public function licenseSaveCallback($value, $dc)
	{
		if ($value !== '' && !static::checkLicense($value)) {
			throw new \Exception($GLOBALS['TL_LANG']['sineos_filemanager_license']['invalidLicense']);
		}

		return $value;
	}
	
	public static function checkLicense($license = null)
	{
		if ($license === null) {
			$license = Config::get('sineos_filemanager_license');
		}

		if (!$license) {
			return false;
		}

		if (in_array(md5($license), static::$validLicenseChecksums, true)) {
			return true;
		}

		return false;
	}
}
