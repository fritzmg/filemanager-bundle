<?php
/*
 * This file is part of sineos-filemanager-bundle.
 *
 * (c) Oliver Lohoff, Contao4you.de
 *
 * @license LGPL-3.0-or-later
 */

namespace Sineos\FileManagerBundle\DataContainer;

use Contao\Backend;
use Contao\StringUtil;
use Contao\Image;
use Sineos\FileManagerBundle\LicenseHelper;

class FileListener extends Backend
{
    public function getGlobalNoUsageButton($href, $label, $title, $class, $attributes, $table, $ids)
    {
        if(LicenseHelper::checkLicense()) {
            return '<a class="header_icon" href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . $label . '</a>';
        }
    }

    public function getNoUsageButton($arrData, $href, $label, $title, $icon, $attributes, $table, $ids, $childIds)
    {
        if(LicenseHelper::checkLicense() && $arrData["type"] == "folder") {
            //return '<a class="header_icon" href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . $label . '</a>';
            $href = $href . "&amp;id=" . $arrData["id"];
            return '<a href="' . $this->addToUrl($href) . '">' . Image::getHtml($icon, $label) . '</a>';
        }
    }
}