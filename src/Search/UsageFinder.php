<?php

namespace Sineos\FileManagerBundle\Search;

use Contao\Database;
use Sineos\FileManagerBundle\LicenseHelper;
use Contao\Controller;
use Contao\StringUtil;
use Contao\FilesModel;

class UsageFinder
{
    public function findUsagesByFile($objFile)
    {
        $db = Database::getInstance();
        $tables = $db->prepare("SHOW TABLES")->execute();

		$arrReturn = array();
		$arrReturn['id'] = $objFile->id;
		$arrReturn['uuid'] = StringUtil::binToUuid($objFile->uuid);
		$arrReturn['name'] = $objFile->name;
		$arrReturn['path'] = $objFile->path;
		$arrReturn['extension'] = $objFile->extension;
		$arrReturn['usages'] = array();

        while ($tables->next()) {
            $row = $tables->row();
            $table = array_shift($row);
			$skip_tables = array('tl_version', 'tl_undo', 'tl_files', 'tl_search', 'tl_search_index');
			if(in_array($table,$skip_tables)) {
				continue;
			}
			$columns = $db->prepare("SHOW COLUMNS FROM ".$table)->execute();

			$columns_array = array();
			while ($columns->next()) {
				if(strpos($columns->Type,"binary")!==false) {
					$columns_array[] = "`".$columns->Field."` = UNHEX('".bin2hex($objFile->uuid)."')";
				}

				if(strpos($columns->Type,"blob")!==false) {
					$columns_array[] = "`".$columns->Field."` LIKE '%".$objFile->uuid."%'";
				}				
			}

			if(!$columns_array) {
				continue;
			} else {
				$query = "SELECT id FROM ".$table." WHERE ".implode(" OR ",$columns_array);
			}

			$usage = $db->prepare($query)->query();
			if($usage->count() > 0) {
				while ($usage->next()) {
					$temp_array = array();
					$temp_array['id'] = $usage->id;

					if($table == 'tl_content') {
						$content = $db->prepare("SELECT pid, ptable, type FROM tl_content WHERE id = ".$usage->id)->query();
						$temp_array['type'] = $GLOBALS['TL_LANG']['CTE'][$content->type][0];
						if($content->ptable == 'tl_article') {
							$article = $db->prepare("SELECT pid FROM tl_article WHERE id = ".$content->pid)->query();
							$temp_array['page'] = $article->pid;
						}

						if($content->ptable == 'tl_calendar_events') {
							$action = '?do=calendar&table='.$table.'&id='.$usage->id;
						} elseif($content->ptable == 'tl_news') {
							$action = '?do=news&table='.$table.'&id='.$usage->id;
						} elseif($content->ptable == 'tl_faq') {
							$action = '?do=faq&table='.$table.'&id='.$usage->id;
						} else {
							$action = '?do=article&table='.$table.'&id='.$usage->id;
						}
					} elseif($table == 'tl_calendar_events') {
						$action = '?do=calendar&table='.$table.'&id='.$usage->id;
					} elseif($table == 'tl_news') {
						$action = '?do=news&table='.$table.'&id='.$usage->id;
					} elseif($table == 'tl_faq') {
						$action = '?do=faq&table='.$table.'&id='.$usage->id;
					}

					if(LicenseHelper::checkLicense()) {
						$temp_array['table'] = $table;
						list($width, $height, $type, $attr) = getimagesize($objFile->path);
						$temp_array['details'] = number_format(filesize($objFile->path) / 1048576, 2) . 'MB';
						if($width) {
							$temp_array['details'].= ' ('.$width.'x'. $height.'px)';
						}
						if($action) {
							$temp_array['action'] = '<a href="contao'.$action.'&act=edit&rt='.Controller::replaceInsertTags('{{request_token}}').'"><img src="system/themes/flexible/icons/edit.svg" width="16" height="16" alt=""></a>';
							$temp_array['action'] .= ' <a href="contao?do=files&act=edit&id='.$objFile->path.'&rt='.Controller::replaceInsertTags('{{request_token}}').'"><img src="system/themes/flexible/icons/sizes.svg" width="16" height="16" alt=""></a>';
						}
					}
					$arrReturn['usages'][] = $temp_array;
				}
			}
		}
        return $arrReturn;
    }

	public function findUsageById($id)
	{
		$objFile = FilesModel::findById($id);
		return $this->findUsagesByFile($objFile);
	}
}