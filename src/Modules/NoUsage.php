<?php
/*
 * This file is part of sineos-filemanager-bundle.
 *
 * (c) Oliver Lohoff, Contao4you.de
 *
 * @license LGPL-3.0-or-later
 */

namespace Sineos\FileManagerBundle\Modules;

use Contao\Backend;
use Contao\BackendTemplate;
use Contao\Controller;
use Contao\Database;
use Contao\FilesModel;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Sineos\FileManagerBundle\LicenseHelper;

class NoUsage extends Backend {

	protected $strTemplate = 'be_filemanager_nousage';


	public function showNoUsage() {

		System::loadLanguageFile('sineos_filemanager');

		$GLOBALS['TL_CSS'][] = 'bundles/filemanager/css/filemanager.css';

		$path = "files%";
		$id = Input::get("id");
        if(isset($id) && $id !== "") {
            $path = urlencode($id . "%");
		}

		if(LicenseHelper::checkLicense() == false) {
			$this->Template = new BackendTemplate("be_maintenance");
			$this->Template->content = '<p>Keine gültige Lizenz</p>';
			$this->Template->href = $this->getReferer(true);
			$this->Template->title = StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['backBTTitle']);
			$this->Template->button = $GLOBALS['TL_LANG']['MSC']['backBT'];
			
			return $this->Template->parse();
		}

        $this->Template = new BackendTemplate($this->strTemplate);
		$this->Template->path = $path;
		$this->Template->href = $this->getReferer(true);
		$this->Template->title = StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['backBTTitle']);
		$this->Template->button = $GLOBALS['TL_LANG']['MSC']['backBT'];

		return $this->Template->parse();
	}

	protected function fileIsUsed($db, $tables, $objFile)
	{
		$tables->reset();
		while ($tables->next()) {
            $row = $tables->row();
            $table = array_shift($row);
			$skip_tables = array('tl_version', 'tl_undo', 'tl_files', 'tl_search', 'tl_search_index');
			if(in_array($table,$skip_tables)) {
				continue;
			}
			$columns = $db->prepare("SHOW COLUMNS FROM ".$table)->execute();

			$columns_array = array();
			while ($columns->next()) {
				if(strpos($columns->Type,"binary")!==false) {
					$columns_array[] = "`".$columns->Field."` = UNHEX('".bin2hex($objFile->uuid)."')";
				}

				if(strpos($columns->Type,"blob")!==false) {
					$columns_array[] = "`".$columns->Field."` LIKE '%".$objFile->uuid."%'";
				}				
			}

			if(!$columns_array) {
				continue;
			} else {
				$query = "SELECT id FROM ".$table." WHERE ".implode(" OR ",$columns_array);
			}

			$usage = $db->prepare($query)->query();

			if($usage->count() > 0) {
				return true;
			}
		}

		return false;
	}
}
