<?php
namespace Sineos\FileManagerBundle\Modules;

use Contao\Backend;
use Contao\BackendTemplate;
use Contao\Controller;
use Contao\Database;
use Contao\FilesModel;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Sineos\FileManagerBundle\Search\UsageFinder;
use Sineos\FileManagerBundle\LicenseHelper;

class Usage extends Backend {

	protected $strTemplate = 'be_filemanager_usage';
	protected $usageFinder;

	public function __construct()
	{
		$this->usageFinder = new UsageFinder();
	}

    public function showUsage() {

		if(!Input::get('id')) {
			return '';
		}

		$GLOBALS['TL_CSS'][] = 'bundles/filemanager/css/filemanager.css';

		$objFile = FilesModel::findByPath(Input::get('id'));
		$fileInfos = $this->usageFinder->findUsagesByFile($objFile);

		System::loadLanguageFile('sineos_filemanager');
        $this->Template = new BackendTemplate($this->strTemplate);
		$this->Template->fileInfos = $fileInfos;
		return $this->Template->parse();
	}
}
