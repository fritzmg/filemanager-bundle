<?php

use Sineos\FileManagerBundle\Modules\Usage;
use Sineos\FileManagerBundle\Modules\NoUsage;

$GLOBALS['BE_MOD']['system']['files']['usage'] = array(Usage::class, 'showUsage');
$GLOBALS['BE_MOD']['system']['files']['nousage'] = array(NoUsage::class, 'showNoUsage');
