<?php

$GLOBALS['TL_LANG']['sineos_filemanager']['invalidLicense'] = 'Bitte geben Sie einen gültigen Lizenzschlüssel ein.';
$GLOBALS['TL_LANG']['sineos_filemanager']['noUsage'] = 'Die Datei wird aktuell nicht verwendet.';
$GLOBALS['TL_LANG']['sineos_filemanager']['table'] = 'Tabelle';
$GLOBALS['TL_LANG']['sineos_filemanager']['type'] = 'Element-Typ';
$GLOBALS['TL_LANG']['sineos_filemanager']['id'] = 'ID';
$GLOBALS['TL_LANG']['sineos_filemanager']['preview'] = 'Vorschau';
$GLOBALS['TL_LANG']['sineos_filemanager']['extension'] = 'Dateiendung';
$GLOBALS['TL_LANG']['sineos_filemanager']['details'] = 'Details';
$GLOBALS['TL_LANG']['sineos_filemanager']['page'] = 'Seite';
$GLOBALS['TL_LANG']['sineos_filemanager']['action'] = 'Aktionen';